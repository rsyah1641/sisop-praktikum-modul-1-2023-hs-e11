#!/bin/bash
# login retep.sh

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

function main {
  #"Login System"
  #"Masukan Username dan Password"

  read -p "uname: " username
  read -p "pass: " password

  #check username
  if [[ ! "$(grep "$uname" "$users_list")" ]] ; then
    	echo -e "\nUsername atau password salah"
    exit 1
  fi

  #check log in
  if [[ "$(grep "$uname" "$users_list" | cut -d ':' -f 2)" == "$pass" ]] ; then
    	echo -e "\nLogin berhasil"
    	log "INFO" "User $uname has logged in"
  else
    	echo -e "\nLogin gagal"
	log "ERROR" "Login has failed on user $uname"
    exit 1
  fi
}

main

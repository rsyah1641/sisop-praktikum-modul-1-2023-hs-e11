#!/bin/bash
echo -e "\n\n*** Tampilkan 5 Universitas dengan ranking tertinggi di Jepang ***"
grep 'Japan' university_data.csv | sort -n -t , -k 1 | head -n 5 | awk -F "," '{print $1","$2","$3}'

#soal 1B
echo -e "\n\n*** mencari Faculty Student Score yang paling rendah diantara 5 Universitas di Jepang ***"
grep 'Japan' university_data.csv | sort -n -t , -k 1 | head -n 5 | sort -t, -k 9 | head -n 1 | awk -F "," '{print $2","$9}'

#Soal 1C
echo -e "\n\n*** cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi ***"
grep 'Japan' university_data.csv | sort -n -t , -k 20 | head -n 10 | awk -F "," '{print $2","$20}'

#Soal 1D
echo -e "\n*** universitas dengan kata kunci 'keren' ***"
grep -i "keren" university_data.csv | awk -F "," '{print $1","$2","$3}'

# sisop-praktikum-modul-1-2023-HS-E11



## Anggota Kelompok E11

Sastiara Maulikh(5025201257)

Muhammad Febriansyah (5025211164)

Schaquille Devlin Aristano (5025211211)

# Soal 1
Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi 


## Soal 1 bagian 1
Tampilkan 5 Universitas dengan ranking tertinggi di Jepang

## Cara Penyelesaian
Digunakan syntac ```grep``` untuk mengambil baris yang terdapat kata 'japan' dan dilanjutkan menggukanan ```sort``` untuk menyortir dari kolom pertama ```-k 1```, lalu untuk mengambil 5 baris teratas dari hasil yang di sort di berikan syntac ``` head -n 5``` dan ```awk -F "," '{print $1","$2","$3}``` guna untuk menampilkan hasil dari kolom pertama, kedua, dan ketiga yang dipisahkan oleh koma (",").

## Source Code
```sh
#!/bin/bash
echo -e "\n\n*** Tampilkan 5 Universitas dengan ranking tertinggi di Jepang ***"
grep 'Japan' university_data.csv | sort -n -t , -k 1 | head -n 5 | awk -F "," '{print $1","$2","$3}'
```

## Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/output-1A.png)

# Soal 1 bagian 2
Keluarkan output Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang

### Cara Penyelesaian
penggunaan metode hampir sama dengan soal 1 bagian 1 namun karena di sort lagi untuk menemukan nilai frs score yang rendah yang mana nilai frs terdapat pada kolom 9, sehingga ditambahkan```sort -t , -k 9``` dan ```head -n 1``` untuk menampilkan satu univ saja.

### Source Code
```sh
#!/bin/bash
echo -e "\n\n*** mencari Faculty Student Score yang paling rendah diantara 5 Universitas di Jepang ***"
grep 'Japan' university_data.csv | sort -n -t , -k 1 | head -n 5 | sort -t, -k 9 | head -n 1 | awk -F "," '{print $2","$9}'
```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/output-1B.png)

## soal 1 bagian 3
Cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi

### Cara Penyelesaian
Digunakan syntac ```grep``` untuk mencari variable 'Japan' pada file csv lalu dilakukan ```sort``` dengan ranking Employment Outcome Rank(ger rank) pada kolom ke 20 dan diberi batasan ```head -n 10``` untuk mengambil 10 universitas teratas. lalu dilanjutkan menggunakan awk untuk mengprint kolom 2 dan kolom 20 sebagai output

### Source Code
```sh
#!/bin/bash
echo -e "\n\n*** cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi ***"
grep 'Japan' university_data.csv | sort -n -t , -k 20 | head -n 10 | awk -F "," '{print $2","$20}'
```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/Screenshot-1231.png)

## Soal 1 bagian 4
Cari universitas tersebut dengan kata kunci keren

### Cara Penyelesaian
digunakan syntax ```grep``` guna untuk mendapatkan data baris yang memiliki kata "keren" , lalu dilanjutkan menggunakan ```awk``` untuk menampilkan hasil output pada kolom 1,2, dan 3

### Source Kode
```sh
echo -e "\n*** universitas dengan kata kunci 'keren' ***"
grep -i "keren" university_data.csv | awk -F "," '{print $1","$2","$3}'
```

### Hasil Output
![image](http://itspreneur.com/wp-content/uploads/2023/03/output-1D.png)

# Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut

### Soal 2 bagian 1
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:

- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat(kumpulan_1, kumpulan_2, dst)

### Cara Penyelesaian

buat suatu tambahan untuk mengetahui bahwa itu adalah perintah untuk melakukan dowload gambar berdasarkan waktu sekarang
```sh
bash university_survey.sh download
```
jika inputan sesuai maka program dapat berjalan

mula-mula buat ```$HOUR``` untuk menyimpan jam sekarang, namun lakukan perintah ```if/else``` untuk mengubah jika terjadi kondisi saat jam saat ini = 0, maka harus dihitung 1. Simpan perubahan pada ```$jumlah_download```

```sh


#mendapatkan data waktu (jam) sekarang
HOUR=$(date +%H)
# Me-Check apabila waktu jam saat ini adalah 0 maka akan dilakukan dowload gambar sebanyak 1 kali
if [ "$HOUR" -eq 0 ]; then
    jumlah_download=1
else
    # Jika tidak, maka akan dilakukan dowload gambar sebanyak waktu jam saat ini!
    jumlah_download="$HOUR"
fi
```

lalu buat sebuah file baru dengan menggukanan syntax ```mkdir``` pada $FileDir ,dengan format ```$FileDir = "kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"```

kemudian lakukan looping (n) dengan batasan 1 sampai kurang dari sama dengan waktu saat ini. dan didalam looping dowload gambar dengan syntax ```wget``` lalu diletakkan pada folder yang telah dibuat kumpulan_[no]/perjalanan_$n.png

### Source Code
```sh

if [ "$1" = "download" ]; then 
#memasukkan kedalam folder "kumpulan_(no folder)"
FileDir="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
    mkdir "$FileDir"

# Looping sesuai waktu
for ((p=1; p<=$jumlah_download; p++))
do
  # Download gambar
  FileNama="perjalanan_$p"
  echo "$filename"
  wget http://itspreneur.com/wp-content/uploads/2023/03/$FileNama.jpg -O ./$FileDir/$FileNama.jpg
done
```

## Soal 2 bagian 2

Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas

### Cara Penyelesaian

buat suatu tambahan untuk mengetahui bahwa itu adalah perintah untuk melakukan force zip pada suatu folder , sbb :
```sh
bash university_survey.sh zip
```

simpan input pada ```$1```. dan digunakan ```if/else``` untuk menceknya. didalam if/else terdapat ```$JumlahDevil =$(ls -l | grep -c "devil_[0-9]*\.zip")``` guna untuk mencek/menghitung jumlah zip yang sudah terdapat dalam folder. lalu nama folder akan +1 sesuai dengan format penamaan yang sudah ditentukan seperti devil_1 dan seterusnya dimana yang di zip adalah folder kumpulan.

### Source Code
``` sh
#jika file diinginkan berupa zip
elif [ "$1" = "zip" ]; then

JumlahDevil=$(ls -l | grep -c "devil_[0-9]*\.zip")
NextDevil="devil_$((JumlahDevil + 1)).zip"
DIRECTORIES = $(find . -type d -name "kumpulan_*")
zip -r $NextDevil $DIRECTORIES

else
    echo "Cara pengisian : bash kobeni_liburan.sh [download/zip]"
    exit 1
fi
```

### All Source Code no 2
``` sh
#!/bin/bash

#mendapatkan data waktu (jam) sekarang
HOUR=$(date +%H)
# Me-Check apabila waktu jam saat ini adalah 0 maka akan dilakukan dowload gambar sebanyak 1 kali
if [ "$HOUR" -eq 0 ]; then
    jumlah_download=1
else
    # Jika tidak, maka akan dilakukan dowload gambar sebanyak waktu jam saat ini!
    jumlah_download="$HOUR"
fi


if [ "$1" = "download" ]; then 
#memasukkan kedalam folder "kumpulan_(no folder)"
FileDir="kumpulan_$(($(ls -d kumpulan_* | wc -l) + 1))"
    mkdir "$FileDir"

# Looping sesuai waktu
for ((p=1; p<=$jumlah_download; p++))
do
  # Download gambar
  FileNama="perjalanan_$p"
  echo "$filename"
  wget http://itspreneur.com/wp-content/uploads/2023/03/$FileNama.jpg -O ./$FileDir/$FileNama.jpg
done

#jika file diinginkan berupa zip
elif [ "$1" = "zip" ]; then

JumlahDevil=$(ls -l | grep -c "devil_[0-9]*\.zip")
NextDevil="devil_$((JumlahDevil + 1)).zip"
DIRECTORIES = $(find . -type d -name "kumpulan_*")
zip -r $NextDevil $DIRECTORIES

else
    echo "Cara pengisian : bash kobeni_liburan.sh [download/zip]"
    exit 1
fi
```

# Soal 3
Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. Peter Griffin juga membuat sistem login yang dibuat di script retep.sh

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut

- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username 
- Tidak boleh menggunakan kata chicken atau ernie

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user.

- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

## Soal 3 bagian 1

### Cara Penyelesaian

### Source Code
```sh
#!/bin/bash
# register louis.sh

#membuat dir
mkdir -p "users"

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'REGISTER:' $1 $2" >> log.txt
}

function main {

  #Registration System
  #Masukan username dan password baru

  read -p "uname: " username

  read -p "pass: " password

  if [[ "$(grep "$uname" "$users_list")" ]]; then
    	echo -e "\nUsername sudah terdaftar"
    	log "ERROR" "User already exists"
    exit 1
  fi

  if [[ "${#password}" -lt 8 ]]; then
    	echo -e "\npass minimal 8 karakter"
    exit 1
  fi

  if [[ ! $(echo "$pass" | grep '[a-z]' | grep '[A-Z]') ]]; then
    	echo -e "\pPass harus memiliki minimal 1 huruf kapital dan 1 huruf kecil"
    exit 1
  fi

  if [[ "$pass" =~ [^a-zA-Z0-9] ]]; then
    	echo -e "\npass harus menggunakan karakter alphanumeric"
    exit 1
  fi

  if [[ "$pass" == "$uname" ]]; then
    	echo -e "\npass tidak boleh sama dengan username"
    exit 1
  fi

  if [[ $(echo "$pass" | grep -i 'chicken') ]] || [[ $(echo "$pass" | grep -i 'ernie') ]]; then
    	echo -e "\npass tidak dapat menggunakan kata chicken atau ernie"
    exit 1
  fi

  echo "$uname:$pass" >> "$users_list"
  echo -e "\nAkun telah dibuat"
  log "INFO" "User $uname registered"
}

main

```

## Soal 3 bagian 2
### Cara Penyelesaian
### Source Code
```sh
#!/bin/bash
# login retep.sh

users_list="users/users.txt"

function log {
  echo "$(date +'%Y/%m/%d %H:%M:%S') 'LOGIN:' $1 $2" >> log.txt
}

function main {
  #"Login System"
  #"Masukan Username dan Password"

  read -p "uname: " username
  read -p "pass: " password

  #check username
  if [[ ! "$(grep "$uname" "$users_list")" ]] ; then
    	echo -e "\nUsername atau password salah"
    exit 1
  fi

  #check log in
  if [[ "$(grep "$uname" "$users_list" | cut -d ':' -f 2)" == "$pass" ]] ; then
    	echo -e "\nLogin berhasil"
    	log "INFO" "User $uname has logged in"
  else
    	echo -e "\nLogin gagal"
	log "ERROR" "Login has failed on user $uname"
    exit 1
  fi
}

main
```

# Soal 4
Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet       yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

### Cara Penyelesaian
digunakan ```$(date +'%H')``` untuk mengambil waktu dan ```$(cat /var/log/syslog)``` untuk membaca file syslog. lalu, digunakan loop untuk encrypt dan decrypt isi file

### source code encrypt.sh
```sh
#!/bin/bash

#jam dalam format 24H
jam=$(date +'%H')
jam=${jam#0}

syslog=$(cat /var/log/syslog)

encrypted=""
echo ${#syslog}
for ((i=0; i<${#syslog}; i++)); do
    char="${syslog:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 + $jam) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 + $jam) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    encrypted="${encrypted}${char}"
done

#filename dalam format hour:minute date:month:year
filename=$(date +'%H:%M %d:%m:%Y').txt
echo -e "$encrypted" > "./$filename"

```
### source Code decrypt.sh
``` sh
#!/bin/bash

#filename menggunakan argument
filename="$1"
jam="${filename:0:2}"

encrypted=$(cat "$filename")

decrypted=""
for ((i=0; i<${#encrypted}; i++)); do
    char="${encrypted:$i:1}"
    if [[ "$char" =~ [A-Za-z] ]]; then
        if [[ "$char" =~ [A-Z] ]]; then
            charcode=$(( ( $(printf '%d' "'$char") - 65 - $jam + 26) % 26 + 65 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        else
            charcode=$(( ( $(printf '%d' "'$char") - 97 - $jam + 26) % 26 + 97 ))
            char=$(printf \\$(printf '%03o' "$charcode"))
        fi
    fi
    decrypted="${decrypted}${char}"
done

decrypted_filename="${filename%.*}_decrypted.txt"
echo -e "$decrypted" > "$decrypted_filename"
```
